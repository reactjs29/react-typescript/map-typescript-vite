import React from 'react';
import { BtnMyLocation, MapView, SearchBar } from '../components';

const HomeScreen = () => {
  return (
    <div>
      <MapView />
      <BtnMyLocation />
      <SearchBar />
    </div>
  );
};

export { HomeScreen };
