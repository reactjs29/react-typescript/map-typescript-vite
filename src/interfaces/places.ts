export interface PlacesResponse {
  attribution: string;
  features: Feature[];
  query: string[];
  type: string;
}

export interface Feature {
  center: number[];
  context: Context[];
  geometry: Geometry;
  id: string;
  language?: Language;
  language_es?: Language;
  place_name: string;
  place_name_es: string;
  place_type: string[];
  properties: Properties;
  relevance: number;
  text: string;
  text_es: string;
  type: string;
}

export interface Context {
  id: string;
  language?: Language;
  language_es?: Language;
  short_code?: ShortCode;
  text: string;
  text_es: string;
  wikidata?: Wikidata;
}

export enum Language {
  Es = 'es',
}

export enum ShortCode {
  PE = 'pe',
  PELma = 'PE-LMA',
}

export enum Wikidata {
  Q2868 = 'Q2868',
  Q419 = 'Q419',
  Q579240 = 'Q579240',
}

export interface Geometry {
  coordinates: number[];
  type: string;
}

export interface Properties {
  address?: string;
  category: string;
  foursquare: string;
  landmark: boolean;
  wikidata?: string;
}
