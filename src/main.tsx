import ReactDOM from 'react-dom/client';
import './index.css';
import MapsApp from './MapsApp';

import mapboxgl from 'mapbox-gl';

mapboxgl.accessToken =
  'pk.eyJ1Ijoid2lzZnRvY2siLCJhIjoiY2t5OHAyMHQzMWh3MTJ3cTlkZ3ljNDgxZCJ9.Casa8l-RE8vSUwr9MwFsjA';

if (!navigator.geolocation) {
  alert('Tu navegador no tiene opcion de geolicalizacion');
  throw new Error();
}

// console.log(navigator.geolocation);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <MapsApp />
);
