import axios from 'axios';

const searchApi = axios.create({
  baseURL: 'https://api.mapbox.com/geocoding/v5/mapbox.places',
  params: {
    limit: 5,
    language: 'es',
    access_token:
      'pk.eyJ1Ijoid2lzZnRvY2siLCJhIjoiY2t5OHAyMHQzMWh3MTJ3cTlkZ3ljNDgxZCJ9.Casa8l-RE8vSUwr9MwFsjA',
  },
});
export default searchApi;
