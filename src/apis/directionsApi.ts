import axios from 'axios';

const directionsApi = axios.create({
  baseURL: 'https://api.mapbox.com/directions/v5/mapbox/driving',
  params: {
    alternatives: true,
    geometries: 'geojson',
    language: 'es',
    overview: 'simplified',
    access_token:
      'pk.eyJ1Ijoid2lzZnRvY2siLCJhIjoiY2t5OHAyMHQzMWh3MTJ3cTlkZ3ljNDgxZCJ9.Casa8l-RE8vSUwr9MwFsjA',
  },
});
export default directionsApi;
