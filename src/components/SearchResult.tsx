import React, { useContext, useState } from 'react';
import { MapContext, PlacesContext } from '../context';
import { Feature } from '../interfaces/places';

const SearchResult = () => {
  const { places, isLoadingPlaces, userLocation } = useContext(PlacesContext);
  const { map, getRouteBetweenPoints } = useContext(MapContext);
  const [activeId, setActiveId] = useState('');

  const handleOnClick = (place: Feature) => {
    const [lng, lat] = place.center;
    setActiveId(place.id);
    map?.flyTo({
      zoom: 15,
      center: [lng, lat],
    });
  };

  const getRoute = (place: Feature) => {
    if (!userLocation) return;
    const [lng, lat] = place.center;
    getRouteBetweenPoints(userLocation, [lng, lat]);
  };

  if (isLoadingPlaces) {
    return (
      <div className='alert alert-primary mt-2 text-center'>
        <h6>Search...</h6>
        <p>stop, please</p>
      </div>
    );
  }

  if (places.length === 0) return <></>;

  return (
    <ul className='list-group mt-3'>
      {places?.map((place) => {
        return (
          <li
            className={`list-group-item list-group-item-action pointer ${
              activeId === place.id && 'active'
            } `}
            onClick={() => handleOnClick(place)}
            key={place.id}
          >
            <h6>{place.text_es}</h6>
            <p style={{ fontSize: '10px' }}>{place.place_name}</p>
            <button
              className={`btn btn-outline-primary ${
                activeId === place.id
                  ? 'btn-primary text-white'
                  : 'btn-outline-primary'
              }`}
              onClick={() => getRoute(place)}
            >
              Direcciones
            </button>
          </li>
        );
      })}
    </ul>
  );
};

export { SearchResult };
