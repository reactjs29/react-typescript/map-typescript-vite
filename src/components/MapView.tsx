import { useContext, useLayoutEffect, useRef } from 'react';
import mapboxgl from 'mapbox-gl';
import { PlacesContext, MapContext } from '../context';
import { Loading } from './Loading';

const MapView = () => {
  const { isLoading, userLocation } = useContext(PlacesContext);
  const { setMap } = useContext(MapContext);
  const mapDiv = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    if (!isLoading) {
      const map = new mapboxgl.Map({
        container: mapDiv.current!,
        style: 'mapbox://styles/mapbox/dark-v10',
        center: userLocation,
        zoom: 15,
      });
      setMap(map);
    }
  }, [isLoading]);

  if (isLoading) {
    return <Loading />;
  }

  return (
    <div
      ref={mapDiv}
      style={{
        //   backgroundColor: 'coral',
        height: '100vh',
        width: '100vw',
        top: 0,
        left: 0,
      }}
    >
      {userLocation?.join(',')}
    </div>
  );
};

export { MapView };
