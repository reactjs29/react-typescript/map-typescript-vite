import React, { useContext } from 'react';
import { MapContext, PlacesContext } from '../context';

export const BtnMyLocation = () => {
  const { map, isMapReady } = useContext(MapContext);
  const { userLocation } = useContext(PlacesContext);

  const handleClick = () => {
    if (!isMapReady) throw new Error('Mapa no esta listo');
    if (!userLocation) throw new Error('No hay ubicacion del usuario');

    map?.flyTo({
      zoom: 15,
      center: userLocation,
    });
  };

  return (
    <button
      className='btn btn-primary'
      onClick={handleClick}
      style={{ position: 'fixed', top: '20px', right: '20px', zIndex: '999' }}
    >
      Retornar mi ubicacion
    </button>
  );
};
