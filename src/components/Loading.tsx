import React from 'react';

const Loading = () => {
  return (
    <div className='loadign-map d-flex justify-content-center align-items-center'>
      <div className='text-center'>
        <h3>Stop please</h3>
        <span>Location</span>
      </div>
    </div>
  );
};

export { Loading };
